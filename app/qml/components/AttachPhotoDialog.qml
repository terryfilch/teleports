import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import "../actions"

PopupDialog {
    property var files
    property var caption
    text: i18n.tr("Do you want to attach this in compressed or uncompressed quality?")
    confirmButtonColor: theme.palette.normal.activity
    confirmButtonText: i18n.tr("Compressed picture")
    cancelButtonText: i18n.tr("Uncompressed picture")
    onConfirmed: AppActions.chat.sendPhoto(files, caption);
    onCanceled: AppActions.chat.sendDocument(files, caption);
}